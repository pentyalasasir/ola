package com.ola.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ola.project.entity.CarIssue;
import com.ola.project.repository.ICarIssueDAO;

@Service
public class CarIssueService implements ICarIssueService{
	
	@Autowired
	ICarIssueDAO carIssueDAO;

	@Override
	public List<CarIssue> getCarIssues() {
		// TODO Auto-generated method stub
		System.out.println("service start");
		return carIssueDAO.findAll();
	}

}
