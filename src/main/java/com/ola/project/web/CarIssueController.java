package com.ola.project.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ola.project.entity.CarIssue;
import com.ola.project.service.ICarIssueService;

@RestController
@RequestMapping("/api/issues")
public class CarIssueController {
	
	@Autowired
	ICarIssueService carIssueService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CarIssue>> getAllCarIssues(){
	
		System.out.println("controller start");
		List<CarIssue> list = carIssueService.getCarIssues();
		System.out.println("controller stop "+ list.toString());
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}	

}
